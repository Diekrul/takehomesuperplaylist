import { Library, UPDATE_LIBRARY } from "./types";

export function updateLibrary(library:Library) {
  return {
    type: UPDATE_LIBRARY,
    payload: library
  };
}
