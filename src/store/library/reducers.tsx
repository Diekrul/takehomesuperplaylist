import { UPDATE_LIBRARY, Library, UpdateLibraryAction } from "./types";

const initialState: Library = {
    id:"1",
    playlists:[{
      id:"1",
      name:"Roadtripe",
      color:"#ffcc80",
      songs:[{id:"1", name:"Pink Floyd: Wish you where here"}],
    },{
      id:"2",
      name:"Work",
      color:"#bcaaa4",
      songs:[{id:"2", name:"Pink Floyd: Echoes"}, {id:"3", name:"Radio Head: Street Spirit"}],
    },{
      id:"3",
      name:"Relaxation",
      color:"#b0bec5",
      songs:[{id:"4", name:"Bob Marley: Natural Mystic"}],
    }],
  }

export function libraryReducer(state = initialState, action: UpdateLibraryAction): Library {
  switch (action.type) {
    case UPDATE_LIBRARY: {
      return {
        ...state,
        ...action.payload,
      }
    }
    default:
      return state;
  }
}
