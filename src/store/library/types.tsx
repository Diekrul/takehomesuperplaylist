
import { Playlist } from './../playlist/types'

export interface Library {
  id: string;
  playlists: Playlist[];
}

export const UPDATE_LIBRARY = "UPDATE_LIBRARY";

export interface UpdateLibraryAction {
  type: typeof UPDATE_LIBRARY;
  payload: Library;
}
