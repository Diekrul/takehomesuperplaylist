import { SET_PLAYLIST, Playlist, SetPlaylistAction } from "./types";

const initialState: Playlist = {
    id:"",
    name:"",
    color:"",
    songs:[],
  }

export function playlistReducer(state = initialState, action: SetPlaylistAction): Playlist {
  switch (action.type) {
    case SET_PLAYLIST: {
      return {
        ...state,
        id: action.payload.id,
        name: action.payload.name,
        color: action.payload.color,
        songs: action.payload.songs,
      }
    }
    default:
      return state;
  }
}
