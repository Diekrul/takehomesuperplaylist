import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';

import { AppState } from '../store';
import { Playlist } from '../store/playlist/types';
import { Song } from '../store/song/types';
import { Library } from '../store/library/types';
import { setPlaylist } from '../store/playlist/actions'
import { updateLibrary } from '../store/library/actions'

const ticket = require("./../../public/images/ticket3.png");

interface IProps {
  navigation: NavigationScreenProp<any, any>;
  library: Library;
  playlist: Playlist;
  setPlaylist: typeof setPlaylist;
  updateLibrary: typeof updateLibrary;
  allSongs: Song[]
}

interface IState {
  listOfSongs: { song: Song, isSelected: Boolean }[];
  selectedCounter: number;
}

class AddSong extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      listOfSongs: [],
      selectedCounter: 0,
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({ done: () => this.done() })
    this.refreshList()
  }

  refreshTitleCount() {
    this.props.navigation.setParams({ selectedCounter: `${this.state.selectedCounter}` })
  }

  refreshList() {
    const listOfSongsWithState = this.listOfSongsWithState()
    this.setState({
      listOfSongs: listOfSongsWithState,
      selectedCounter: this.countSelectedSongs(listOfSongsWithState)
    }, () => this.refreshTitleCount())
  }

  countSelectedSongs(list: { song: Song, isSelected: Boolean }[]): number {
    let selectedSongs = 0
    list.forEach(element => {
      if (element.isSelected) {
        selectedSongs++;
      }
    });
    return selectedSongs
  }

  listOfSongsWithState(): { song: Song, isSelected: Boolean }[] {
    const { allSongs, playlist } = this.props
    let listOfSongsWithState = allSongs.map((song) => ({ song: song, isSelected: false }))
    listOfSongsWithState.forEach((element) => {
      playlist.songs.forEach((songOfPlaylist: Song) => {
        if (element.song.id == songOfPlaylist.id) {
          element.isSelected = true;
        }
      });
    })
    return listOfSongsWithState
  }

  done() {
    var updatedPlaylist = this.updateCurrentPlaylist();
    var newLibrary: Library = { id: this.props.library.id, playlists: [] };
    this.props.library.playlists.forEach(playlist => {
      if (playlist.id == updatedPlaylist.id) {
        playlist = updatedPlaylist;
      }
      newLibrary.playlists.push(playlist)
    });
    this.props.updateLibrary(newLibrary);
    this.props.navigation.navigate('Playlist',{selectedPlaylist:updatedPlaylist})
  }

  updateCurrentPlaylist(): Playlist {
    const selectedSongs: Song[] = [];
    this.state.listOfSongs.forEach(element => {
      if (element.isSelected) {
        selectedSongs.push(element.song);
      }
    });
    const playlist = this.props.playlist;
    playlist.songs = selectedSongs;
    this.props.setPlaylist(playlist);
    return playlist;
  }

  pressSong(item: { song: Song, isSelected: Boolean }, listOfSongs: { song: Song, isSelected: Boolean }[]) {
    var modifyCounter = item.isSelected ? 1 : -1
    var selectedCounter = this.state.selectedCounter - modifyCounter;
    for (let index = 0; index < listOfSongs.length; index++) {
      if (listOfSongs[index].song.id == item.song.id) {
        listOfSongs[index].isSelected = !listOfSongs[index].isSelected
        break;
      }
    }
    this.setState({ listOfSongs, selectedCounter }, () => this.refreshTitleCount())
  }

  render() {
    var { listOfSongs } = this.state
    return (
      <View style={[styles.container, { backgroundColor: this.props.playlist.color }]}>
        <FlatList
          ItemSeparatorComponent={() => <View style={styles.line} />}
          data={listOfSongs}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) =>
            <TouchableOpacity onPress={() => this.pressSong(item, listOfSongs)}>
              <View style={styles.row}>
                <Text style={styles.name}>{item.song.name}</Text>
                {item.isSelected ?
                  <View style={styles.iconContainer}>
                    <Image source={ticket} style={styles.icon} resizeMode='cover' />
                  </View> : null}
              </View>
            </TouchableOpacity>
          }
          keyExtractor={item => item.song.id}
        />
      </View>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    playlist: state.playlistReducer,
    library: state.libraryReducer,
    allSongs: state.songReducer
  }
}

export default connect(mapStateToProps, { setPlaylist, updateLibrary })(AddSong)

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  name: {
    flex: 1,
    display: 'flex',
    margin: 25,
    fontSize: 18,
    fontWeight: 'bold',
  },
  icon: {
    marginTop: 10,
    height: 56,
    width: 56,
  },
  iconContainer: {
    height: 24,
    width: 24,
    marginRight: 20,
    alignItems: 'center',
    alignContent: 'center',
    textAlign: 'center'
  },
  line: {
    height: 1,
    width: "100%",
    backgroundColor: "grey"
  },
  row: {
    flex: 1,
    height: 70,
    flexDirection: 'row'
  },
})
