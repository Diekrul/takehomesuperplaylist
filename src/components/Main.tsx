import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';

import { AppState } from '../store';
import { setPlaylist } from '../store/playlist/actions'
import { Playlist } from '../store/playlist/types';
import { Library } from '../store/library/types';

interface IProps {
  navigation: NavigationScreenProp<any, any>
  setPlaylist: typeof setPlaylist;
  library: Library;
}

const goToPlaylist = (props: IProps, item: Playlist) => {
  props.setPlaylist(item);
  props.navigation.navigate('Playlist', { selectedPlaylist: item });
};

const Main = (props: IProps) => {
  return (
    <View style={styles.container}>
      <FlatList
        data={props.library.playlists}
        showsVerticalScrollIndicator={false}
        renderItem={({ item }) =>
          <TouchableOpacity onPress={() => goToPlaylist(props, item)}>
            <View style={[styles.row, { backgroundColor: item.color }]}>
              <Text style={styles.name}>{item.name}</Text>
              <View style={styles.countContainer}>
                <Text style={styles.count}>({item.songs.length})</Text>
              </View>
            </View>
          </TouchableOpacity>
        }
        keyExtractor={item => item.id}
      />
    </View>
  )
}

const mapStateToProps = (state: AppState) => {
  return {
    library: state.libraryReducer,
  }
}

export default connect(mapStateToProps, { setPlaylist })(Main)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  row: {
    height: 70,
    flex: 1,
    flexDirection: 'row'
  },
  rowCol: {
    flex: 1,
    flexDirection: "column"
  },
  name: {
    flex: 1,
    display: 'flex',
    margin: 20,
    fontSize: 18,
    fontWeight: 'bold',
  },
  count: {
    marginTop: 5,
    marginRight: 5,
    fontSize: 24,
  },
  countContainer: {
    margin: 15,
    height: 70,
    width: 50,
    alignItems: 'center',
    alignContent: 'center',
    textAlign: 'center'
  },
})