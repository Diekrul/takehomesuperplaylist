import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';

import { AppState } from '../store';
import { Playlist as PlaylistType } from '../store/playlist/types';

interface IProps {
  navigation: NavigationScreenProp<any, any>;
  playlist: PlaylistType;
}

const Playlist = (props: IProps) => {
  const { playlist } = props
  var songs = playlist.songs
  var color = playlist.color
  return (
    <View style={[styles.container, { backgroundColor: color }]}>
      <FlatList
        ItemSeparatorComponent={() => <View style={styles.line} />}
        data={songs}
        showsVerticalScrollIndicator={false}
        renderItem={({ item }) =>
          <View style={styles.row}>
            <Text style={styles.name}>{item.name}</Text>
          </View>
        }
        keyExtractor={item => item.id}
      />
    </View>
  )
}

const mapStateToProps = (state: AppState) => {
  return {
    playlist: state.playlistReducer,
  }
}

export default connect(mapStateToProps)(Playlist)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  name: {
    flex: 1,
    display: 'flex',
    margin: 20,
    fontSize: 18,
    fontWeight: 'bold',
  },
  line: {
    height: 1,
    width: "100%",
    backgroundColor: "grey"
  },
  row: {
    flex: 1,
    height: 70,
  }
})

