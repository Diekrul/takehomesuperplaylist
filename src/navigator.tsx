import { TouchableOpacity, Image, Text } from "react-native";
import { createStackNavigator, createAppContainer, NavigationScreenProps } from "react-navigation";
import React from "react";
import Login from './components/Login';
import Main from './components/Main';
import Playlist from './components/Playlist';
import AddSong from './components/AddSong';

const add = require("./../public/images/add_blue.png");

const AppNavigator = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      headerLeft: null,
      header: null,
    }
  },
  Main: {
    screen: Main,
    navigationOptions: {
      title: "Playlists"
    },
  },
  Playlist: {
    screen: Playlist,
    navigationOptions: ({ navigation }: NavigationScreenProps) => ({
      title: navigation.state.params ? navigation.state.params.selectedPlaylist.name : '',
      headerRight:
        <TouchableOpacity
          onPress={() => navigation.navigate('AddSong', {
            selectedCounter: navigation.state.params ?
              navigation.state.params.selectedPlaylist.songs.length :
              null
          })}>
          <Image source={add} style={{ margin: 10, height: 24, width: 24 }} resizeMode='contain' />
        </TouchableOpacity>
    }),
  },
  AddSong: {
    screen: AddSong,
    navigationOptions: ({ navigation }: NavigationScreenProps) => ({
      title: navigation.state.params ?
        `Selected Songs ${navigation.state.params.selectedCounter}` :
        "Selected Songs",
      headerRight:
        <TouchableOpacity onPress={navigation.getParam('done')}
          accessibilityLabel="decrement"
          style={{ alignItems: "center", justifyContent: "center", width: 70 }}>
          <Text style={{fontSize: 16,color: "#087f23"}}>Done</Text>
        </TouchableOpacity>
    }),
  },
});
export default createAppContainer(AppNavigator);
