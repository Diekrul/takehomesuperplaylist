
import React, { Component } from 'react';
import AppNavigator from './src/navigator';
import { Provider } from "react-redux";
import { store } from "./src/store/index";

console.disableYellowBox = true;
export default class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    )
  }
}